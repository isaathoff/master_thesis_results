This project contains the result archive of my master thesis. 

Included are the matrices and pulse profiles of NuSTAR and HXMT data, as well as 
the pulse profile fitting with the RXTE template.

For more information, I refer to my thesis or you can contact me directly under
saathoff@astro.uni-tuebingen.de
